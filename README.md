# MyTextField

[![CI Status](https://img.shields.io/travis/豆子信息_dengjie/MyTextField.svg?style=flat)](https://travis-ci.org/豆子信息_dengjie/MyTextField)
[![Version](https://img.shields.io/cocoapods/v/MyTextField.svg?style=flat)](https://cocoapods.org/pods/MyTextField)
[![License](https://img.shields.io/cocoapods/l/MyTextField.svg?style=flat)](https://cocoapods.org/pods/MyTextField)
[![Platform](https://img.shields.io/cocoapods/p/MyTextField.svg?style=flat)](https://cocoapods.org/pods/MyTextField)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MyTextField is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MyTextField', :git => "https://gitee.com/djGit/MyTextField.git"
```

## Author

豆子信息_dengjie, jie.deng@douziit.com

## License

MyTextField is available under the MIT license. See the LICENSE file for more info.
