//
//  main.m
//  MyTextField
//
//  Created by 豆子信息_dengjie on 12/12/2018.
//  Copyright (c) 2018 豆子信息_dengjie. All rights reserved.
//

@import UIKit;
#import "DJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DJAppDelegate class]));
    }
}
